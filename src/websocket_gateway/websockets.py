from typing import List, Dict

from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from fastapi.responses import HTMLResponse
import json
import time

app = FastAPI()

html = """
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
    </head>
    <body>
        <h1>WebSocket Chat</h1>
        <h2>Your ID: <span id="ws-id"></span></h2>
        <form action="" onsubmit="sendMessage(event)">
            ChatID: <input type="text" id="chatID" autocomplete="off"/>
            Message: <input type="text" id="messageText" autocomplete="off"/>
            <button>Send</button>
        </form>
        <ul id='messages'>
        </ul>
        <script>
            var client_id = Date.now()
            document.querySelector("#ws-id").textContent = client_id;
            var ws = new WebSocket(`ws://localhost:8001/ws/${client_id}`);
            ws.onmessage = function(event) {
                var messages = document.getElementById('messages')
                var message = document.createElement('li')
                var content = document.createTextNode(event.data)
                message.appendChild(content)
                messages.appendChild(message)
            };
            function sendMessage(event) {
                var message = document.getElementById("messageText")
                var eChatID = document.getElementById("chatID")
                chatID = eChatID.value
                if(chatID == ""){
                    chatID = "all"
                }
                data = {
                    chat_id: chatID,
                    message: message.value
                }
                console.log(data)
                if(message.value == ""){
                    event.preventDefault()
                    return
                }
                ws.send(JSON.stringify(data))
                message.value = ''
                event.preventDefault()
            }
        </script>
    </body>
</html>
"""


class ConnectionManager:
    def __init__(self):
        self.active_connections: Dict[int, WebSocket] = dict()

    async def connect(self, client_id: int, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.update({client_id: websocket})

    def disconnect(self, client_id: int):
        self.active_connections.pop(client_id)

    def get_connection(self, client_id: int):
        return self.active_connections.get(client_id)

    async def send_personal_message(self, client_id: int, message: str):
        websocket = self.active_connections.get(client_id)
        if websocket:
            await websocket.send_text(message)

    async def broadcast(self, message: str):
        for connection in self.active_connections.values():
            await connection.send_text(message)


manager = ConnectionManager()


@app.get("/")
async def get():
    return HTMLResponse(html)


@app.websocket("/ws/{client_id}")
async def websocket_endpoint(websocket: WebSocket, client_id: int):
    await manager.connect(client_id, websocket)
    try:
        while True:
            text = await websocket.receive_text()
            print(f"retrived message: {text} from {client_id}")
            data = json.loads(text)
            chat_id = data.get("chat_id")
            message_content = data.get("message", "")
            if chat_id == "all":
                await manager.broadcast(
                    f"(public) Client #{client_id} says: {message_content}"
                )
            else:
                chat_id = int(chat_id)
                message = f"(from {client_id} to you): {message_content}"
                await manager.send_personal_message(chat_id, message)
            successMessage = (
                f"You sent message: {message_content} to {chat_id}"
            )
            await manager.send_personal_message(client_id, successMessage)
            time.sleep(0.0001)
    except WebSocketDisconnect:
        manager.disconnect(client_id)
        await manager.broadcast(f"Client #{client_id} left the chat")
